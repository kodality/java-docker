# Java OpenJDK 17 base image
## Features
1. Importing ssl certificates from mounted volume /certs before running an application
2. Includes APP_VERSION from docker --build-arg parameter
3. Supports JAVA_OPTS env parameter
4. Supports custom scripts before running java (from /scripts)
