FROM openjdk:17-slim

RUN apt-get update && apt-get install curl telnet iputils-ping -y

RUN update-ca-certificates -f

ADD scripts /scripts
ADD entrypoint.sh /

RUN mkdir /certs

ENTRYPOINT ["/entrypoint.sh"]
