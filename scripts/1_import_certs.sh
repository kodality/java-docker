#!/usr/bin/env bash
if [[ ! -z "$(ls -A /certs)" ]]; then
  for f in /certs/* ; do
    if [[ $f == *.p12 ]]; then
      echo "adding keystore $f"
      keytool -v -importkeystore -cacerts -srckeystore $f -srcstoretype PKCS12 -srcstorepass changeit -deststorepass changeit
    else
      echo "importing cert $f"
      keytool -import -cacerts -storepass changeit -noprompt -alias $(basename "$f") -file $f
    fi
  done
fi
